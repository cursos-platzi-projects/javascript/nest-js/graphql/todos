import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(
    new ValidationPipe({ // Se usa para en el inputType poner validaciones de los campos
      whitelist: true,
      forbidNonWhitelisted: true, 
    })
  );

  await app.listen(3000);
}
bootstrap();
