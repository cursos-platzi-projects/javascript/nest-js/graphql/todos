import { Field, Int, ObjectType } from '@nestjs/graphql';


/** @ObjectType = para definir que es un objeto  */
@ObjectType({ description: 'Todo quick aggregations' })
export class AggregationsType {

    @Field( () => Int )
    total: number;

    @Field( () => Int )
    pending: number;

    @Field( () => Int )
    completed: number;

    /** deprecationReason = Cuando quieres agregar algo obsoleto a la documentacion */
    @Field( () => Int, { deprecationReason: 'Most use completed instead' })
    totalTodosCompleted: number;

}