import { ArgsType, Field } from "@nestjs/graphql";
import { IsBoolean, IsOptional } from "class-validator";

/**
 * @ArgsType() = Definicion de argumento con validaciones
 */
@ArgsType()
export class StatusArgs {

    @Field( () => Boolean, { nullable: true })
    @IsOptional()
    @IsBoolean()
    status?: boolean;

}