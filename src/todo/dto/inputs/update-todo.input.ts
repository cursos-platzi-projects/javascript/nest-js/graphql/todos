import { Field, InputType, Int } from '@nestjs/graphql';
import { IsBoolean, IsInt, IsNotEmpty, IsOptional, IsString, MaxLength, Min } from 'class-validator';

/** @InputType = Para definir un objeto que sera el request, donde tambien tendra validaciones */
@InputType()
export class UpdateTodoInput {

    @Field( () => Int )
    @IsInt() // Debe ser entero
    @Min(1) // Minimo 1 caracter
    id: number;


    @Field( () => String, { description: 'What needs to be done', nullable: true })
    @IsString() // Debe ser tipo String
    @IsNotEmpty() // No debe estar vacio en el request
    @MaxLength(20) // Maximo de caracteres 20
    @IsOptional() // El valor es opcional
    description?: string;

    @Field( () => Boolean, { nullable: true })
    @IsOptional()
    @IsBoolean() // Debe ser booleano
    done?: boolean; // ? = significa que puede ser opcional


}