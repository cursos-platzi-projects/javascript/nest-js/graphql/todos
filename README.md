# Nest Js - GraphQL

## Instalacion

1. Se clona el proyecto a la carpeta deseada.
2. Se instala las dependencias
`npm install`
3. Se puede ya correr el proyecto con
`npm run start:dev`

## Apollo Client
### Parametros en los metodos

    {
      #(variableName: Valor)
      todos (status: true) {
        description
        done
        id
      }
    }

-------------

Renombrar metodos que salga como resultado en el response, similiar to 'AS' en SQL

    {
      #Nombre del resultado: Objeto definido
      completados: todos {
        description
        done
        id
      }
    }

-------------

#### Fragments

Sin Fragments

    {
      completados: todos {
        description
        done
        id
      }
    
      incopletos: todos {
        description
        done
        id
      }
    }

 Con Fragments

    {
      completados: todos {
        ...fields
      }
    
      incompletos: todos {
        ...fields
      }
    }

    fragment fields on Todo {
      description
      done
      id
    }